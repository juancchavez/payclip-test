package com.test.playclip.paycliptest.detail.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.test.playclip.paycliptest.R
import com.test.playclip.paycliptest.databinding.ActivityDetailBinding
import com.test.playclip.paycliptest.detail.viewmodel.DetailActivityViewModel

class DetailActivity : AppCompatActivity() {
    private lateinit var binding : ActivityDetailBinding
    private val viewModel: DetailActivityViewModel by lazy {
        ViewModelProviders.of(this).get(DetailActivityViewModel::class.java)
    }
    private val adapter: DetailAdapter by lazy {
        DetailAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        intent.extras?.getString("tlvString")?.let {
            viewModel.setTlvValue(it)
        }
        binding.list.adapter = adapter

        viewModel.action().observe(this, Observer {
            when (it) {
                is DetailActivityViewModel.Action.UpdateDetailsList -> adapter.setData(it.items)
                is DetailActivityViewModel.Action.DisplayDecodeError -> binding.txtvError.visibility = View.VISIBLE
            }
        })
    }
}