package com.test.playclip.paycliptest.home.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import com.test.playclip.paycliptest.R
import com.test.playclip.paycliptest.databinding.ActivityHomeBinding
import com.test.playclip.paycliptest.detail.view.DetailActivity
import com.test.playclip.paycliptest.home.viewmodel.HomeActivityViewModel

class HomeActivity : AppCompatActivity() {
    private lateinit var binding : ActivityHomeBinding
    private val viewModel by lazy {
        ViewModelProviders.of(this).get(HomeActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =  DataBindingUtil.setContentView(this, R.layout.activity_home)
        binding.btnExample1.setOnClickListener {
            binding.textInputEditText.setText(getString(R.string.tlv_example_1))
        }
        binding.btnExample2.setOnClickListener {
            binding.textInputEditText.setText(getString(R.string.tlv_example_2))
        }
        binding.btnDecode.isEnabled = false
        binding.btnDecode.setOnClickListener {
            binding.textInputEditText.text?.let { newText ->
                viewModel.onDecodeClick(newText.toString())
            }
        }
        binding.textInputEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.btnDecode.isEnabled = !s.isNullOrEmpty() && !s.isNullOrBlank()
            }

        })
        viewModel.action().observe(this, Observer {
            when (it) {
                is HomeActivityViewModel.Action.DisplayParsedDetails -> {
                    val intent = Intent(this, DetailActivity::class.java)
                    intent.putExtra("tlvString", it.value)
                    startActivity(intent)
                }
            }
        })
    }
}