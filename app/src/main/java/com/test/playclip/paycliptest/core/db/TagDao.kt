package com.test.playclip.paycliptest.core.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface TagDao {
    @Query("SELECT * FROM tag")
    fun getAllTags(): List<Tag>

    @Query("SELECT * FROM tag WHERE tagId = :id")
    fun getTagById(id: String): List<Tag>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tag: List<Tag>)
}