package com.test.playclip.paycliptest

import android.app.Application
import android.content.Context

class PayClipTestApplication : Application() {

    companion object {
        private var instance: PayClipTestApplication? = null
        fun applicationContext() : Context {
            return instance!!
        }
    }

    init {
        instance = this
    }
}