package com.test.playclip.paycliptest.detail.data

import android.arch.lifecycle.MutableLiveData
import com.payneteasy.tlv.BerTlv
import com.test.playclip.paycliptest.PayClipTestApplication
import com.test.playclip.paycliptest.core.db.Tag
import com.test.playclip.paycliptest.core.db.TagDatabase
import com.test.playclip.paycliptest.detail.model.TagDetailModel

class TagRepository {
    private val db = TagDatabase.getInstance(PayClipTestApplication.applicationContext())
    val tagResults1: MutableLiveData<List<TagDetailModel>> = MutableLiveData()

    fun getTagInfo(berTlvList: List<BerTlv>) {
        val result = ArrayList<TagDetailModel>()
        val dao = db?.getTagDao()
        berTlvList.forEach { berTlv ->
            val tagId = berTlv.tag.toString().replace("- ", "").toLowerCase()
            dao?.getTagById(tagId)?.firstOrNull().let { tag ->
                if (tag != null) {
                    val id = tag.tagId
                    val description = tag.description
                    val value = when (tag.tagType) {
                        Tag.TYPE_TEXT -> berTlv.textValue
                        else -> berTlv.hexValue
                    }
                    result.add(TagDetailModel(id, description, value))
                } else {
                    result.add(TagDetailModel.getUnknownTag(tagId, berTlv.hexValue))
                }
            }
        }
        tagResults1.postValue(result)
    }
}