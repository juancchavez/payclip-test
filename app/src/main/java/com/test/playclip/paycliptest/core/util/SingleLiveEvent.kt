package com.test.playclip.paycliptest.core.util

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.annotation.AnyThread
import android.support.annotation.MainThread
import java.util.concurrent.atomic.AtomicBoolean

class SingleLiveEvent<T> : MutableLiveData<T>() {

    private val pending: AtomicBoolean = AtomicBoolean(false)
    private val observersMap: MutableMap<Observer<T>, Observer<T>> = mutableMapOf()

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<T>) {
        super.observe(owner, Observer<T> {
            if (pending.compareAndSet(true, false)) {
                observer.onChanged(it)
            }
        }.also {
            observersMap.put(observer, it)
        })
    }

    override fun removeObserver(observer: Observer<T>) {
        super.removeObserver(observersMap.getOrElse(observer, { observer }))
    }

    @MainThread
    override fun setValue(value: T?) {
        pending.set(true)
        super.setValue(value)
    }

    @MainThread
    fun call() {
        value = null
    }

    @AnyThread
    fun postCall() {
        postValue(null)
    }
}