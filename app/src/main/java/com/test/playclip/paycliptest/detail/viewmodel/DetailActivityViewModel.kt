package com.test.playclip.paycliptest.detail.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import com.payneteasy.tlv.BerTlvParser
import com.payneteasy.tlv.HexUtil
import com.test.playclip.paycliptest.core.util.SingleLiveEvent
import com.test.playclip.paycliptest.detail.data.TagRepository
import com.test.playclip.paycliptest.detail.model.TagDetailModel

class DetailActivityViewModel : ViewModel() {

    sealed class Action {
        class UpdateDetailsList(val items: List<TagDetailModel>) : Action()
        class DisplayDecodeError: Action()
    }

    private var tagResult: MutableLiveData<List<TagDetailModel>>
    private val action: SingleLiveEvent<Action> = SingleLiveEvent()
    private val tagRepository: TagRepository = TagRepository()
    private val tagResultObserver = Observer<List<TagDetailModel>> {
        it?.let {
            if (it.isNotEmpty()) {
                action.value = Action.UpdateDetailsList(
                        it
                )
            }
        }
    }

    init {
        tagResult = tagRepository.tagResults1
        tagResult.observeForever(tagResultObserver)
    }

    override fun onCleared() {
        super.onCleared()
        tagResult.removeObserver(tagResultObserver)
    }

    fun setTlvValue(it: String) {
        val bytes = HexUtil.parseHex(it)
        try {
            BerTlvParser().parse(bytes, 0, bytes.size)?.let {
                tagRepository.getTagInfo(it.list)
            }
        } catch (ex: Exception) {
            action.value = Action.DisplayDecodeError()
        }
    }

    fun action() = action
}