package com.test.playclip.paycliptest.detail.model

class TagDetailModel (
    var id: String,
    var description: String,
    var value: String
) {
    companion object {
        fun getUnknownTag(id: String, value: String) = TagDetailModel(
                id, "Unknown Tag", value
        )
    }
}