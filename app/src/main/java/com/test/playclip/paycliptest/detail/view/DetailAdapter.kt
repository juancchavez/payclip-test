package com.test.playclip.paycliptest.detail.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.test.playclip.paycliptest.databinding.ItemTlvElementBinding
import com.test.playclip.paycliptest.detail.model.TagDetailModel
import java.util.ArrayList

class DetailAdapter : RecyclerView.Adapter<DetailAdapter.ViewHolder>() {

    private val values: MutableList<TagDetailModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        ItemTlvElementBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(values[position])
    }

    fun setData(items: List<TagDetailModel>) {
        values.addAll(items)
        notifyDataSetChanged()
    }

    class ViewHolder(val item: ItemTlvElementBinding) : RecyclerView.ViewHolder(item.root) {
        fun bind(tlvItem: TagDetailModel) {
            item.txtvKey.text = tlvItem.id
            item.txtvDescription.text = tlvItem.description
            item.txtvValue.text = tlvItem.value
        }
    }
}