package com.test.playclip.paycliptest.home.viewmodel

import android.arch.lifecycle.ViewModel
import com.test.playclip.paycliptest.core.util.SingleLiveEvent

class HomeActivityViewModel : ViewModel() {

    private val action: SingleLiveEvent<Action> = SingleLiveEvent()

    sealed class Action {
        class DisplayParsedDetails (val value: String) : Action()
    }

    fun onDecodeClick(value: String) {
        if (value.isEmpty()) return
        action.value = Action.DisplayParsedDetails(value)
    }

    fun action() = action
}