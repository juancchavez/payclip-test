package com.test.playclip.paycliptest.core.db

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.Room
import android.content.Context
import java.util.concurrent.Executors


@Database(
        entities = [Tag::class],
        version = 1
)
abstract class TagDatabase : RoomDatabase() {
    companion object {
        private var instance: TagDatabase? = null

        fun getInstance(context: Context): TagDatabase? {
            if (instance == null) {
                instance = Room.databaseBuilder(context,
                        TagDatabase::class.java,
                        "tagDatabase.db")
                        .allowMainThreadQueries()
                        .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)
                                Executors.newSingleThreadScheduledExecutor().execute {
                                    getInstance(context)?.getTagDao()?.insert(Tag.tags())
                                }
                            }
                        })
                        .build()
            }
            return instance
        }
    }

    abstract fun getTagDao(): TagDao
}