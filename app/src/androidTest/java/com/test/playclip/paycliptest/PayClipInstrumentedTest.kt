package com.test.playclip.paycliptest

import android.support.test.espresso.Espresso
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.test.playclip.paycliptest.home.view.HomeActivity
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class PayClipInstrumentedTest {

    @Rule
    @JvmField var activityRule: ActivityTestRule<HomeActivity> = ActivityTestRule(HomeActivity::class.java)

    @Test
    fun initialStateTest() {
        Espresso.onView(withId(R.id.textInputEditText)).check(matches(withText("")))
        Espresso.onView(withId(R.id.btn_decode)).check(matches(not(isEnabled())))
        Espresso.onView(withId(R.id.btn_example_1)).perform(click())
        Espresso.onView(withId(R.id.textInputEditText)).check(matches(withText(R.string.tlv_example_1)))
        Espresso.onView(withId(R.id.btn_decode)).check(matches(isEnabled()))
    }
}